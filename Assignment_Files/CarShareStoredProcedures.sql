﻿BEGIN TRANSACTION;
DROP FUNCTION IF EXISTS carsharing.newbooking(character varying,character varying,character varying,interval);
DROP FUNCTION IF EXISTS carsharing.updatehomebay(character varying,character varying);
DROP FUNCTION IF EXISTS carsharing.logincheck(character varying,character varying);
DROP VIEW IF EXISTS CarSharing.WeekendRatio;
DROP VIEW IF EXISTS CarSharing.BookingFrequency;
DROP VIEW IF EXISTS CarSharing.BookingRecency;
DROP FUNCTION IF EXISTS carsharing.GetAnalysis();
DROP INDEX IF EXISTS carsharing.GetCarBay;
DROP TABLE IF EXISTS carsharing.Reservation;
DROP TRIGGER IF EXISTS UpdateReservation on CarSharing.Booking;
END;

--Login Check shared proc to check both username or email
CREATE OR REPLACE FUNCTION CarSharing.LoginCheck(identifier VARCHAR(200), pword VARCHAR(200)) RETURNS TABLE(nickname VARCHAR(10),nameTitle VARCHAR(10),nameGiven VARCHAR(100),nameFamily VARCHAR(100),address VARCHAR(200),name VARCHAR(80),since date,subscribed VARCHAR(20),stat_nrbookings integer, email emailtype) AS
$$
BEGIN
	RETURN QUERY (SELECT member.nickname,member.nameTitle,member.nameGiven,member.nameFamily,member.address,carbay.name,member.since,member.subscribed,member.stat_nrOfBookings,member.email
                 FROM carsharing.member JOIN carsharing.carbay on (homebay = bayID)
                 WHERE (member.email=identifier OR member.nickname=identifier) AND member.password=pword);
		
END;
$$ LANGUAGE plpgsql;

--Stored Procedure for adding a new booking
CREATE OR REPLACE FUNCTION CarSharing.NewBooking(emailAddress VARCHAR(200),car_rego VARCHAR(6), startTimeStamp VARCHAR(50), duration interval) returns boolean AS
$$
DECLARE
	memberId INT;
	StartTS TIMESTAMP;
	endTimeStamp TIMESTAMP;
	BEGIN
		
		SELECT memberno into memberId FROM Member WHERE member.email = emailAddress;
		SELECT(SELECT to_timestamp(startTimeStamp, 'YYYY-MM-DD HH24:MI:SS')) INTO StartTS;
		SELECT StartTS  + duration into endTimeStamp;
		--Cant have a booking before the current date
		IF StartTS < DATE_TRUNC('hour',NOW()+interval '1 hour')::timestamp THEN
			RETURN false;
		--Can't have a booking by the same member for an overlapping time with an existing booking
		ELSIF EXISTS (SELECT 1 FROM Booking WHERE madeby = memberId AND StartTS <= booking.endTime AND endTimeStamp >= booking.startTime) THEN 
			RETURN false;
		--Cant have a booking for one car with overlapping times
		ELSIF EXISTS (SELECT 1 FROM Booking WHERE StartTS <= booking.endTime AND endTimeStamp >= booking.startTime AND car = car_rego) THEN
			RETURN false;
			
		--Succes, insert booking into database
		ELSE
			INSERT INTO carsharing.Booking(car, madeby, whenBooked, startTime, endTime) VALUES (car_rego,memberId,DATE_TRUNC('second', NOW())::timestamp,StartTS,endTimeStamp);
			IF EXISTS (SELECT 1 FROM carsharing.booking WHERE car = car_rego AND startTime = StartTS AND endTime = endTimeStamp) THEN
				UPDATE carsharing.member SET stat_nrOfBookings = stat_nrOfBookings + 1 WHERE member.email = emailAddress;
			END IF;
			RETURN true; 
		END IF;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION CarSharing.UpdateHomeBay(userEmail VARCHAR(50), bayName VARCHAR(80)) RETURNS boolean as
$$
DECLARE
bayNo INT;
BEGIN
	SELECT bayID into bayNo FROM CarBay WHERE CarBay.name = bayName;
	UPDATE carsharing.Member SET homebay = bayNo WHERE member.email = userEmail;
	IF EXISTS (SELECT homebay FROM carsharing.member WHERE email = userEmail AND homebay = bayNo) THEN
		return true;
		COMMIT;
	ELSE
		return false;
		ROLLBACK;
	END IF;
END;
$$ LANGUAGE plpgsql;

--Stored Procedures for Extension 4
CREATE VIEW CarSharing.WeekendRatio AS
SELECT email, NumWeekend, NumWeekday, (SELECT CASE WHEN (NumWeekend/2)>(NumWeekday/7) THEN 'Weekend' ELSE 'Weekday' END)as Ratio
FROM (
SELECT email, COUNT(startTime) as NumWeekend
FROM CarSharing.Member JOIN CarSharing.Booking ON (memberno = madeby)
WHERE EXTRACT(ISODOW FROM startTime)>= 6
GROUP BY email) as Weekend NATURAL JOIN 
(SELECT email, COUNT(startTime) as NumWeekday
FROM CarSharing.Member JOIN CarSharing.Booking ON (memberno = madeby)
WHERE EXTRACT(ISODOW FROM startTime)<= 5
GROUP BY email) as Weekday;

CREATE VIEW CarSharing.BookingFrequency AS
SELECT email, bookingsperday, NTILE(5) over(order by BookingsPerDay asc)FreqQuintile FROM (
SELECT email, (NumBookings*10000/MemberDays) as BookingsPerDay FROM (
SELECT email, (MAX(startTime::date)-MIN(startTime::date)) as MemberDays, COUNT(startTime) as NumBookings
FROM carsharing.member JOIN carsharing.booking on (memberno = madeby)
GROUP BY email) as BookingStats
order by bookingsperday desc) as BookingRank;

CREATE VIEW CarSharing.BookingRecency AS
SELECT email, TimeSinceLast, NTILE(5) over(ORDER BY TimeSinceLast DESC)RecQuintile FROM
(SELECT email, (CURRENT_TIMESTAMP - MAX(startTime)) as TimeSinceLast
FROM carsharing.member JOIN carsharing.booking on (memberno = madeby)
WHERE startTime <= CURRENT_TIMESTAMP
GROUP BY email) as Recency;

CREATE OR REPLACE FUNCTION CarSharing.GetAnalysis() RETURNS TABLE(nname VARCHAR, freq integer, recent integer,type text) as
$$
BEGIN
	RETURN QUERY SELECT member.nickname as nname, FreqQuintile as freq, RecQuintile as recent, Ratio as type
	FROM carsharing.member NATURAL JOIN CarSharing.BookingFrequency NATURAL JOIN CarSharing.BookingRecency NATURAL JOIN CarSharing.WeekendRatio
	ORDER BY nname asc;
END;
$$ LANGUAGE plpgsql;

CREATE INDEX GetCarBay ON CarSharing.CarBay USING hash(name); --For get_bay method in databases.py - hash because it is an equality search
--END Stored Procedures for Extension 4

--Table and stored procs for extension 1
CREATE TABLE CarSharing.Reservation(
	car RegoType NOT NULL,
	reservedFrom TIMESTAMP NOT NULL,
	reservedUntil TIMESTAMP NOT NULL,
	CONSTRAINT Reservation_PK PRIMARY KEY(car, reservedFrom, reservedUntil)
);


--Triggeer function to create reservation table entry from new booking
CREATE OR REPLACE FUNCTION CarSharing.insertToReservation() RETURNS TRIGGER AS 
$$
BEGIN
	INSERT INTO CarSharing.Reservation VALUES(NEW.car, NEW.startTime, NEW.endTime);
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

--Trigger for updating reservation table
CREATE TRIGGER UpdateReservation AFTER INSERT ON Carsharing.Booking
FOR EACH ROW EXECUTE PROCEDURE insertToReservation();
--END Tables and stored procs for extension 1