#!/usr/bin/env python3

from modules import pg8000
import configparser
import math


# Define some useful variables
ERROR_CODE = 55929

#####################################################
##  Database Connect
#####################################################

def database_connect():
    # Read the config file
    config = configparser.ConfigParser()
    config.read('config.ini')

    # Create a connection to the database
    connection = None
    try:
        connection = pg8000.connect(database=config['DATABASE']['user'],
            user=config['DATABASE']['user'],
            password=config['DATABASE']['password'],
            host=config['DATABASE']['host'])
    except pg8000.OperationalError as e:
        print("""Error, you haven't updated your config.ini or you have a bad
        connection, please try again. (Update your files first, then check
        internet connection)
        """)
        print(e)
    #return the connection to use
    return connection

#####################################################
##  Login
#####################################################

def check_login(email, password):
    # Dummy data
    val = ['Shadow', 'Mr', 'Evan', 'Nave', '123 Fake Street, Fakesuburb', 'SIT', '01-05-2016', 'Premium', '1']
    
    # Check if the user details are correct!
    # Return the relevant information (watch the order!)
    # Ask for the database connection, and get the cursor set up
    conn = database_connect()
    if(conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
        # Try executing the SQL and get from the database
        sql = """SELECT * FROM CarSharing.LoginCheck(%s, %s);"""
        cur.execute(sql, (email, password))
        r = cur.fetchone()
        conn.commit() 
        cur.close()                     # Close the cursor
        conn.close()                    # Close the connection to the db
        return r
    except:
        # If there were any errors, return a NULL row printing an error to the debug
        print("Error with Database")
    conn.rollback()
    cur.close()                     # Close the cursor
    conn.close()                    # Close the connection to the db
    return None


#####################################################
##  Homebay
#####################################################
def update_homebay(email, bayname):

    # Update the user's homebay
    conn = database_connect()
    if(conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
        # Try executing the SQL and get from the database
        sql = """SELECT CarSharing.UpdateHomeBay(%s, %s);"""
        cur.execute(sql, (email, bayname))
        r = cur.fetchone()
        conn.commit() 
        cur.close()                     # Close the cursor
        conn.close()                    # Close the connection to the db
        return r
    except:
        # If there were any errors, return a NULL row printing an error to the debug
        print("Error with Database")
    conn.rollback() 
    cur.close()                     # Close the cursor
    conn.close()                    # Close the connection to the db
    return None

#####################################################
##  Booking (make, get all, get details)
#####################################################

def make_booking(email, car_rego, date, hour, duration):
    # Insert a new booking
    # Make sure to check for:
    #       - If the member already has booked at that time
    #       - If there is another booking that overlaps
    #       - Etc.
    # return False if booking was unsuccessful :)
    # We want to make sure we check this thoroughly
    startTime = date + ' ' + hour +':00:00'
    durationTS = duration + ':00:00'
    
    conn = database_connect()
    if(conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
        print(email)
        print(car_rego)
        print(startTime)
        print(durationTS)
        # Try executing the SQL and get from the database
        sql = """SELECT carsharing.NewBooking(%s, %s, %s, %s);"""
        #Set isolation level to serializable to prevent a car being booked concurrently
        cur.execute("""SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;""");
        cur.execute(sql, (email, car_rego, startTime, durationTS))
        r = cur.fetchone()
        conn.commit() 
        cur.close()                     # Close the cursor
        conn.close()                    # Close the connection to the db
        
        return r[0]                     # Return the boolean value from the stored procedure
    except:
        # If there were any errors, return a NULL row printing an error to the debug
        print("Error with Database")
    conn.rollback()
    cur.close()                     # Close the cursor
    conn.close()                    # Close the connection to the db
    return None


def get_all_bookings(email):
    #A default value at the start of the assignment
    val = [['66XY99', 'Ice the Cube', '01-05-2016', '10', '4', '29-04-2016'],['66XY99', 'Ice the Cube', '27-04-2016', '16'], ['WR3KD', 'Bob the SmartCar', '01-04-2016', '6']]

    # Get all the bookings made by this member's email
    conn = database_connect()
    if(conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
        # Try executing the SQL and get from the database
        sql = """SELECT car, car.name, startTime::date, date_part('hour', startTime)
                 FROM (carsharing.member JOIN carsharing.booking on(madeBy = memberNo)) JOIN carsharing.car on (regno = car)
                 WHERE member.email = %s
                 ORDER BY startTime desc, date_part desc;"""
        cur.execute(sql, (email,))
        r = cur.fetchall()
        conn.commit() 
        cur.close()                     # Close the cursor
        conn.close()                    # Close the connection to the db
        return r
    except:
        # If there were any errors, return a NULL row printing an error to the debug
        print("Error with Database")
    conn.rollback()
    cur.close()                     # Close the cursor
    conn.close()                    # Close the connection to the db
    return None

def get_booking(b_date, b_hour, car):
    #A default value at the start of the assignment
    val = ['Shadow', '66XY99', 'Ice the Cube', '01-05-2016', '10', '4', '29-04-2016', 'SIT']

    # Get the information about a certain booking
    # It has to have the combination of date, hour and car
    startTime = b_date + " %s:00:00" % (int(float(b_hour)))
    print(startTime)
    conn = database_connect()
    if(conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
        # Try executing the SQL and get from the database
        sql = """SELECT nickname, car.regno, car.name, DATE(startTime), to_char(startTime, 'HH24:MI:SS'), endTime-startTime, DATE(whenBooked), carbay.name
                 FROM (carsharing.member JOIN carsharing.booking on(madeBy = memberNo)) JOIN carsharing.car on (regno = car) JOIN carsharing.carbay on (bayID = parkedAt)
                 WHERE startTime = %s AND car.regno = %s"""
        cur.execute(sql, (startTime, car))
        r = cur.fetchone()
        conn.commit() 
        cur.close()                     # Close the cursor
        conn.close()                    # Close the connection to the db
        return r
    except:
        # If there were any errors, return a NULL row printing an error to the debug
        print("Error with Database")
    conn.rollback()
    cur.close()                     # Close the cursor
    conn.close()                    # Close the connection to the db
    return None

#####################################################
##  Car (Details and List)
#####################################################

def get_car_details(regno):
    #A default value at the start of the assignment
    val = ['66XY99', 'Ice the Cube','Nissan', 'Cube', '2007', 'auto', 'Luxury', '5', 'SIT', '8', 'http://example.com']
    # Get details of the car with this registration number
    # Return the data (NOTE: look at the information, requires more than a simple select. NOTE ALSO: ordering of columns)
    conn = database_connect()
    if(conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
        # Try executing the SQL and get from the database
        sql = """SELECT regno, car.name, make, model, year, transmission, carmodel.category, capacity, carbay.name, walkscore, mapURL
                 FROM carsharing.carmodel NATURAL JOIN carsharing.car JOIN carsharing.carbay ON (parkedAt = bayID)
                 WHERE car.regno = %s;"""
        cur.execute(sql, (regno,))
        r = cur.fetchone()
        conn.commit() 
        cur.close()                     # Close the cursor
        conn.close()                    # Close the connection to the db
        return r
    except:
        # If there were any errors, return a NULL row printing an error to the debug
        print("Error with Database")
    conn.rollback()
    cur.close()                     # Close the cursor
    conn.close()                    # Close the connection to the db
    return None   

def get_all_cars():
    #A default value at the start of the assignment
    val = [ ['66XY99', 'Ice the Cube', 'Nissan', 'Cube', '2007', 'auto'], ['WR3KD', 'Bob the SmartCar', 'Smart', 'Fortwo', '2015', 'auto']]
    # Get all cars that PeerCar has
    # Return the results
    conn = database_connect()
    if(conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
        # Try executing the SQL and get from the database
        sql = """SELECT regno, name, make, model, year, transmission
                 FROM carsharing.car;"""
        cur.execute(sql)
        r = cur.fetchall()
        conn.commit() 
        cur.close()                     # Close the cursor
        conn.close()                    # Close the connection to the db
        return r
    except:
        # If there were any errors, return a NULL row printing an error to the debug
        print("Error with Database")
    conn.rollback()
    cur.close()                     # Close the cursor
    conn.close()                    # Close the connection to the db
    return None    
    
#####################################################
##  Bay (detail, list, finding cars inside bay)
#####################################################

def get_all_bays():
    #A default value at the start of the assignment
    val = [['SIT', '123 Some Street, Boulevard', '2'], ['some_bay', '1 Somewhere Road, Right here', '1']]
    # Get all the bays that PeerCar has :)
    # And the number of bays
    # Return the results
    conn = database_connect()
    if(conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
        # Try executing the SQL and get from the database
        sql = """SELECT carbay.name, carbay.address, COUNT(regno)
                 FROM carsharing.carbay JOIN carsharing.car on (parkedAt = bayID)
                 GROUP BY carbay.name, carbay.address;"""
        cur.execute(sql)
        r = cur.fetchall()
        conn.commit() 
        cur.close()                     # Close the cursor
        conn.close()                    # Close the connection to the db
        return r
    except:
        # If there were any errors, return a NULL row printing an error to the debug
        print("Error with Database")
    conn.rollback()
    cur.close()                     # Close the cursor
    conn.close()                    # Close the connection to the db
    return None 

def get_bay(name):
    #A default value at the start of the assignment
    val = ['SIT', 'Home to many (happy?) people.', '123 Some Street, Boulevard', '-33.887946', '151.192958']

    # Get the information about the bay with this unique name
    # Make sure you're checking ordering ;)
    conn = database_connect()
    if(conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
        # Try executing the SQL and get from the database
        sql = """SELECT carbay.name, carbay.description, carbay.address, gps_lat, gps_long
                 FROM carsharing.carbay
                 WHERE carbay.name = %s;"""
        cur.execute(sql, (name,))
        r = cur.fetchone()
        conn.commit() 
        cur.close()                     # Close the cursor
        conn.close()                    # Close the connection to the db
        return r
    except:
        # If there were any errors, return a NULL row printing an error to the debug
        print("Error with Database")
    conn.rollback()
    cur.close()                     # Close the cursor
    conn.close()                    # Close the connection to the db
    return None 
    
def search_bays(search_term):
    #A default value at the start of the assignment
    val = [['SIT', '123 Some Street, Boulevard', '-33.887946', '151.192958']]

    # Select the bays that match (or are similar) to the search term
    # You may like this
    conn = database_connect()
    if(conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
		##formatting allows for SIMILAR TO search in SQL
        modified_search = "%%%s%%" % (search_term,)
        # Try executing the SQL and get from the database
        sql = """SELECT carbay.name, carbay.address, COUNT(regno)
                 FROM carsharing.carbay JOIN carsharing.car ON(bayID = parkedAt)
                 WHERE LOWER(carbay.name) SIMILAR TO LOWER(%s)
                 GROUP BY carbay.name, carbay.address;"""
        cur.execute(sql, (modified_search,))
        r = cur.fetchall()
        conn.commit() 
        cur.close()                     # Close the cursor
        conn.close()                    # Close the connection to the db
        return r
    except:
        # If there were any errors, return a NULL row printing an error to the debug
        print("Error with Database")
    conn.rollback()
    cur.close()                     # Close the cursor
    conn.close()                    # Close the connection to the db
    return None

def get_cars_in_bay(bay_name):
    #A default value at the start of the assignment
    val = [ ['66XY99', 'Ice the Cube'], ['WR3KD', 'Bob the SmartCar']]

    # Get the cars inside the bay with the bay name
    # Cars who have this bay as their bay :)
    # Return simple details (only regno and name)
    conn = database_connect()
    if(conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
        # Try executing the SQL and get from the database
        sql = """SELECT car.regno, car.name
                 FROM carsharing.carbay JOIN carsharing.car ON (bayID = parkedAt)
                 WHERE carbay.name = %s;"""
        cur.execute(sql, (bay_name,))
        r = cur.fetchall()
        conn.commit() 
        cur.close()                     # Close the cursor
        conn.close()                    # Close the connection to the db
        return r
    except:
        # If there were any errors, return a NULL row printing an error to the debug
        print("Error with Database")
    conn.rollback()
    cur.close()                     # Close the cursor
    conn.close()                    # Close the connection to the db
    return None 

#####################################################
##  Member Analysis
#####################################################   

def get_member_analysis():
    val = [['fakeguy@fakemail.com','3','3','true']]
    
    conn = database_connect()
    if(conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
        # Try executing the SQL and get from the database
        sql = """SELECT * FROM CarSharing.GetAnalysis();"""
        cur.execute(sql)
        r = cur.fetchall()
        conn.commit() 
        cur.close()                     # Close the cursor
        conn.close()                    # Close the connection to the db
        return r
    except:
        # If there were any errors, return a NULL row printing an error to the debug
        print("Error with Database")
    conn.rollback()
    cur.close()                     # Close the cursor
    conn.close()                    # Close the connection to the db
    return None 